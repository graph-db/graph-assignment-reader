package com.righteouscoder.assignmentreader.io;

import com.righteouscoder.assignmentreader.misc.DefaultConstants;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

/**
 * TODO make this a class (with interface?) for reading files for service/etc.
 */
public class FileIOImpl
				implements FileIO {

	private static final Logger
					logger =
					LoggerFactory.getLogger(FileIOImpl.class);


	@Override
	public File getTinkerpopConfigFile()
					throws
					IOException {
		// ClassLoader to get the resource file(s) with.
		ClassLoader classLoader = getClass().getClassLoader();

		// Files for settingsFile to read in directly & tempFile if needed if as
		// a jar.
		File settingsFile = null;
		File tempFile     = null;

		String defaultFileName = DefaultConstants
						.TINKERPOP_DRIVER_SETTINGS_NAME;


		String protocol = FileIOImpl.class
						.getResource(FileIOImpl.class.getSimpleName()
						             + ".class")
						.getProtocol();


		//  Determine if running inside a jar or not & run right code.

		switch (protocol) {
			case "jar":
				logger.trace("running from a jar file");
				InputStream
								is =
								classLoader.getResourceAsStream(defaultFileName);
				tempFile =
								File.createTempFile("graph-reader-",
								                    "-default");
				FileUtils.copyInputStreamToFile(is,
				                                tempFile);
				settingsFile = tempFile;
				tempFile.deleteOnExit();
				break;
			case "file":
				logger.trace("running from class file");
				settingsFile =
								new File(classLoader.getResource(defaultFileName)
								                    .getFile());
			default:
				logger.trace("running from non: jar/file ??");

		}
		return settingsFile;
	}

	@Override
	public InputStream getParserTestFile()
					throws
					IOException {
		// ClassLoader to get the resource file(s) with.
		ClassLoader classLoader = getClass().getClassLoader();

		// Files for settingsFile to read in directly & tempFile if needed if as
		// a jar.
		File settingsFile = null;
		File tempFile     = null;

		String defaultFileName = DefaultConstants
						.PARSER_TEST_FILE_NAME;


		String protocol = FileIOImpl.class
						.getResource(FileIOImpl.class.getSimpleName()
						             + ".class")
						.getProtocol();


		InputStream is = null;
		//  Determine if running inside a jar or not & run right code.

		switch (protocol) {
			case "jar":
				logger.trace("running from a jar file");
				is = classLoader.getResourceAsStream(defaultFileName);

				break;
			case "file":
				logger.trace("running from class file");
				is = classLoader.getResourceAsStream(defaultFileName);

			default:
				logger.trace("running from non: jar/file ??");

		}
		return is;
	}

	public void writeFile(File file,
	                      String content)
					throws
					IOException {

		BufferedWriter bw = null;
		FileWriter     fw = null;

		try {

			fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			bw.write(content);

			System.out.println("Done");

		} finally {


			if (bw
			    != null) {
				bw.close();
			}

			if (fw
			    != null) {
				fw.close();
			}
		}
	}

}
