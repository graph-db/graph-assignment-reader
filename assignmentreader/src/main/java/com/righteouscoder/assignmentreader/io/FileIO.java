package com.righteouscoder.assignmentreader.io;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Interface for FileIOImpl to implement
 */
@SuppressWarnings("UnnecessaryInterfaceModifier")
public interface FileIO {

	public File getTinkerpopConfigFile()
					throws
					IOException;


	public InputStream getParserTestFile()
					throws
					IOException;

	public void writeFile(File file,
	                      String content)
					throws
					IOException;
}
