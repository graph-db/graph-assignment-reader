package com.righteouscoder.assignmentreader.parser;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.misc.Pair;

/**
 *
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class GraphTokenFactory
				implements TokenFactory<GraphToken> {

	/**
	 * Indicates whether {@link CommonToken#setText} should be called after
	 * constructing tokens to explicitly set the text. This is useful for cases
	 * where the input stream might not be able to provide arbitrary substrings
	 * of text from the input after the lexer creates a token (e.g. the
	 * implementation of {@link CharStream#getText} in
	 * {@link UnbufferedCharStream} throws an
	 * {@link UnsupportedOperationException}). Explicitly setting the token text
	 * allows {@link Token#getText} to be called at any time regardless of the
	 * input stream implementation.
	 * <p>
	 * <p>
	 * The default value is {@code false} to avoid the performance and memory
	 * overhead of copying text for every token unless explicitly requested.</p>
	 */
	protected final boolean copyText;

	public GraphTokenFactory(boolean copyText) {
		super();
		this.copyText = copyText;
	}

	/**
	 * Constructs a {@link CommonTokenFactory} with {@link #copyText} set to
	 * {@code false}.
	 */
	public GraphTokenFactory() {
		this(false);
	}

	/**
	 * This is the method used to create tokens in the lexer and in the
	 * error handling strategy. If text!=null, than the start and stop positions
	 * are wiped to -1 in the text override is set in the CommonToken.
	 */
	@Override
	public GraphToken create(Pair<TokenSource, CharStream> source,
	                         int type,
	                         String text,
	                         int channel,
	                         int start,
	                         int stop,
	                         int line,
	                         int charPositionInLine) {
		GraphToken
						t =
						new GraphToken(source,
						               type,
						               channel,
						               start,
						               stop);
		t.setLine(line);
		t.setCharPositionInLine(charPositionInLine);
		if (text
		    != null) {
			t.setText(text);
		} else if (copyText
		           && source.b
		              != null) {
			t.setText(source.b.getText(Interval.of(start,
			                                       stop)));
		}

		return t;
	}

	@Override
	public GraphToken create(int type,
	                         String text) {
		return new GraphToken(type,
		                      text);
	}
}
