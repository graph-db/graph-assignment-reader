package com.righteouscoder.assignmentreader.parser;

import com.righteouscoder.assignmentreader.parser.generated
				.java8.java8BaseListener;
import com.righteouscoder.assignmentreader.parser.generated.java8.java8Parser;
import org.antlr.v4.runtime.RuleContext;

/**
 *
 */

public class MyJava8Listener
				extends java8BaseListener {


	@Override
	public void enterAnnotation(java8Parser.AnnotationContext ctx) {
		printRuleAndText(ctx);
		super.enterAnnotation(ctx);
	}

	@Override
	public void enterReturnStatement(java8Parser.ReturnStatementContext ctx) {
		printRuleAndText(ctx);
		super.enterReturnStatement(ctx);
	}

	void printRuleAndText(RuleContext ctx) {
		System.out.println("rule index: "
		                   + ctx.getRuleIndex()
		                   + " "
		                   + ctx.getText()
		);
	}


}
