package com.righteouscoder.assignmentreader.parser;

import org.antlr.v4.runtime.CommonTokenFactory;
import org.antlr.v4.runtime.ListTokenSource;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenFactory;

import java.util.List;

/**
 *
 */
@SuppressWarnings("unused")
public class GraphTokenSource
				extends ListTokenSource {

	/**
	 * This is the backing field for {@link #getTokenFactory} and
	 */
	private TokenFactory<?> _factory = new GraphTokenFactory(false);

	public GraphTokenSource(List<? extends Token> tokens) {
		super(tokens);

//		for (int i = 0; i
//		                < tokens.size(); i++) {
//
//		}
	}

	public GraphTokenSource(List<? extends Token> tokens,
	                        String sourceName) {
		super(tokens,
		      sourceName);
	}

}
