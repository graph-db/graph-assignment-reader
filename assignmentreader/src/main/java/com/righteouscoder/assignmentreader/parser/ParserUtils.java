package com.righteouscoder.assignmentreader.parser;

import com.righteouscoder.assignmentreader.parser.generated.java8.java8Lexer;
import com.righteouscoder.assignmentreader.parser.generated.java8.java8Parser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class ParserUtils {

	private static final Logger
					logger =
					LoggerFactory.getLogger(ParserUtils.class);


	public static void test(InputStream is)
					throws
					IOException {

		ANTLRInputStream input = new ANTLRInputStream(is);

		System.out.println(""
		                   + input.size());

		java8Lexer lexer = new java8Lexer(input);

		CommonTokenStream tokens = new CommonTokenStream(lexer);

		System.out.println(""
		                   + tokens.getNumberOfOnChannelTokens());


		CommonTokenStream graphTokens = convertLexerTokensToGraphTokens(tokens);

		java8Parser parser = new java8Parser(graphTokens);
//		java8Parser parser = new java8Parser(tokens);

		MyJava8Listener listener = new MyJava8Listener();
		parser.addParseListener(listener);

		ParseTreeWalker walker = new ParseTreeWalker();

		java8Parser.CompilationUnitContext context = parser.compilationUnit();

		walker.walk(listener,
		            context);


		logger.debug("graph token size: "
		             + graphTokens.size());

//		File tempFile =
//						File.createTempFile("graph-reader-",
//						                    "-output");
//
//		FileIO fileIO = new FileIOImpl();
//
//
//		String content = "test";
//		fileIO.writeFile(tempFile,
//		                 content);
//
//		InputStream is2 = new FileInputStream(tempFile);
//
//		Crypto crypto = new CryptoImpl();
//
//		logger.debug("sha1 of test file: "
//		             + crypto.sha1Hex(is2));


//
//		graphTokens.getTokens()
//		           .forEach(ParserUtils::printTokenAsCSV);


	}


	static public CommonTokenStream convertLexerTokensToGraphTokens
					(CommonTokenStream commonTokenStream) {

		logger.debug("commonTokenStream sourceName = "
		             + commonTokenStream.getSourceName());

		List<Token>
						tokenList =
						commonTokenStream.get(0,
						                      commonTokenStream.size());
		logger.debug("commonTokenStream size : "
		             + commonTokenStream.size());
		List<GraphToken> gTokens = new ArrayList<>();
		int              i       = 0;
		for (Token token :
						tokenList) {
			i++;
//			logger.debug("token size : "
//			             + i
//			             + " "
//			             + token.toString());
			gTokens.add(new GraphToken(token));
		}

		Token
						token =
						commonTokenStream.get(commonTokenStream.size()
						                      - 1);


		logger.debug("token ="
		             + token.toString());

		gTokens.add(new GraphToken(token));


		logger.debug("gTokens arraylist size ="
		             + gTokens.size());


		GraphTokenSource graphTokenSource = new GraphTokenSource(gTokens);

//		logger.debug("graphTokenSource size ="
//		             + graphTokenSource.g);

//		graphTokenSource.setTokenFactory(new GraphTokenFactory());

//		graphTokenSource.

		CommonTokenStream cts = null;
		try {
			cts = new CommonTokenStream(graphTokenSource);
			cts.fill();

			logger.debug("cts size = "
			             + cts.size());

			logger.debug("cts size = "
			             + cts.getTokens()
			                  .size());
		} catch
						(Exception ex) {
			logger.error(""
			             + ex.getMessage());
		}


		return cts;

	}


	static public String printString(String string) {
		//System.out.println(string);
		logger.info(string);
		return string;
	}

	static public void printTokenAsCSV(Token token) {
		printString(tokenAsCSV(token));
	}

	static public String tokenAsCSV(Token token) {

		StringBuilder stringBuilder = new StringBuilder();


		String
						// note that this is only done for 'display' purposes.
						tokenText =
						token.getText()
						     .replaceAll("\n",
						                 "<\\\\n>")
						     .replaceAll("\r",
						                 "<\\\\r>");

		stringBuilder
						.append(token.getChannel())
						.append(",")
						.append(token.getTokenIndex())
						.append(",")
						.append(token.getType())
						.append(",")
						.append(token.getLine())
						.append(",")
						.append(token.getStartIndex())
						.append(",")
						.append(token.getStopIndex())
						.append(",\"")
						.append(tokenText)
						.append("\"");

		return stringBuilder.toString();

	}


}
