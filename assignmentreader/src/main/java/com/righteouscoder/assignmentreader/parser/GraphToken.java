package com.righteouscoder.assignmentreader.parser;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonToken;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenSource;
import org.antlr.v4.runtime.misc.Pair;

/**
 * This extneds {@link org.antlr.v4.runtime.CommonToken} and implements
 * the {@link Token} Interface, since CommonToken itself doesn't.
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class GraphToken
				extends CommonToken
				implements Token {


	/**
	 * Constructs a new {@link GraphToken} with the specified token type.
	 *
	 * @param type
	 * 				The token type.
	 */
	public GraphToken(int type) {
		super(type);
	}

	public GraphToken(Pair<TokenSource, CharStream> source,
	                  int type,
	                  int channel,
	                  int start,
	                  int stop) {
		// EMPTY_SOURCE can be used to not tie source to token.
		super(source,
		      type,
		      channel,
		      start,
		      stop);

	}

	/**
	 * Constructs a new {@link GraphToken} with the specified token type and
	 * text.
	 *
	 * @param type
	 * 				The token type.
	 * @param text
	 * 				The text of the token.
	 */
	public GraphToken(int type,
	                  String text) {
		super(type,
		      text);
	}

	/**
	 * Constructs a new {@link GraphToken} as a copy of another {@link Token}.
	 * <p>
	 * <p>
	 * If {@code oldToken} is also a {@link GraphToken} instance, the newly
	 * constructed token will share a reference to the {@link #text} field and
	 * the {@link Pair} stored in {@link #source}. Otherwise, {@link #text} will
	 * be assigned the result of calling {@link #getText}, and {@link #source}
	 * will be constructed from the result of {@link Token#getTokenSource} and
	 * {@link Token#getInputStream}.</p>
	 *
	 * @param oldToken
	 * 				The token to copy.
	 */
	public GraphToken(Token oldToken) {
		super(oldToken);
	}


}
