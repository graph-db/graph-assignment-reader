package com.righteouscoder.assignmentreader.graph_db;

/**
 * Created by Mike on 2/13/2017.
 */

public class GraphSchema {

	public static String NAME          = "graph";
	public static String GRAPH_VERSION = "1";


	public static class vertex {

		public static class school {
		}

		public static class instructor {
		}

		public static class student {
		}

		public static class submission {
		}

		public static class sourceCode {
			public static class property {
				public static class names {
					public final static String CHANNEL        = "Channel";
					public final static String TOKEN_INDEX    = "TokenIndex";
					public final static String TYPE           = "Type";
					public final static String LINE           = "Line";
					public final static String STARTING_INDEX = "StartingIndex";
					public final static String TEXT           = "Text";
				}
			}
		}
	}
}
