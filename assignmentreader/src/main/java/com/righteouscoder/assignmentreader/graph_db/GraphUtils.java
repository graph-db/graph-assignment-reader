package com.righteouscoder.assignmentreader.graph_db;

import org.antlr.v4.runtime.Token;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//import com.thinkaurelius.titan.core.TitanFactory;
//import com.thinkaurelius.titan.core.TitanGraph;

/**
 * Utility class to collect helper methods for interacting with the graph.
 */

@SuppressWarnings("unused")
public class GraphUtils {

	// SLF4J Logger reference for this class.
	private static final Logger
					logger =
					LoggerFactory.getLogger(GraphUtils.class);

	/**
	 * Create Map.Entry of two input String(s).
	 *
	 * @param key
	 * 				String to use as Key.
	 * @param value
	 * 				String to use as Value.
	 *
	 * @return Map.Entry of the two Strings.
	 */
	static public Map.Entry<String, String> createPropertiesTuple(String key,
	                                                              String
					                                                              value) {
		return new AbstractMap.SimpleEntry<>(key,
		                                     value);
	}

	/**
	 * Get the String of the gremlin-script to add a vertex of Token to default
	 * graph: 'graph'.
	 *
	 * @param token
	 * 				Token to convert to a vertex and add.
	 *
	 * @return the Gremlin-Script required to add Token as a vertex to the graph.
	 */
	static public String getAddVertexString(Token token) {

		return getAddVertexString(GraphSchema.NAME,
		                          token);
	}

	/**
	 * Get the String of the gremlin-script to add a vertex of this Token to a
	 * graph.
	 *
	 * @param graphName
	 * 				String of the graph to add the vertex to.
	 * @param token
	 * 				Token to convert to a vertex and add.
	 *
	 * @return the Gremlin-Script required to add Token as a vertex to the graph.
	 */
	static public String getAddVertexString(String graphName,
	                                        Token token) {
		// List of Map.Entry(s)
		List<Map.Entry<String, String>> tuple = new ArrayList<>();
		// add each pair(s) of information.
		tuple.add(createPropertiesTuple(GraphSchema.vertex.sourceCode.property
						                                .names
						                                .CHANNEL,
		                                ""
		                                +
		                                token.getChannel()));
		tuple.add(createPropertiesTuple(GraphSchema.vertex.sourceCode.property
						                                .names
						                                .TOKEN_INDEX,
		                                ""
		                                +
		                                token.getTokenIndex()));
		tuple.add(createPropertiesTuple(GraphSchema.vertex.sourceCode.property
						                                .names
						                                .TYPE,
		                                ""
		                                + token
						                                .getType()));
		tuple.add(createPropertiesTuple(GraphSchema.vertex.sourceCode.property
						                                .names
						                                .LINE,
		                                ""
		                                + token
						                                .getLine()));
		tuple.add(createPropertiesTuple(GraphSchema.vertex.sourceCode.property
						                                .names
						                                .STARTING_INDEX,
		                                ""
		                                + token.getStartIndex()));
		tuple.add(createPropertiesTuple(GraphSchema.vertex.sourceCode.property
						                                .names
						                                .TEXT,
		                                ""
		                                + token
						                                .getText()));
		// get the gremlin-script String and return it.
		return getAddVertexString(graphName,
		                          tuple);
	}

	/**
	 * Get the String of the gremlin-script to add a vertex to the default graph
	 * named 'graph'.
	 *
	 * @param pairs
	 * 				List of String pairs.
	 *
	 * @return gremlin query string to add vertex to graph.
	 */
	static public String getAddVertexString(List<Map.Entry<String, String>>
					                                        pairs) {
		// Call actual implementation method with default graph name.
		return getAddVertexString(GraphSchema.NAME,
		                          pairs);
	}

	/**
	 * Get the String of the gremlin-script to add a vertex to a graph.
	 *
	 * @param graphName
	 * 				Name of the graph to add vertex to.
	 * @param pairs
	 * 				List of String pairs.
	 *
	 * @return gremlin query string to add vertex to graph.
	 */
	static public String getAddVertexString(String graphName,
	                                        List<Map.Entry<String, String>>
					                                        pairs) {
		// string constants for "accessor tokens"
		String vertexId    = "T.id";
		String vertexLabel = "T.label";
		// use StringBuilder to be more efficient.
		StringBuilder stringBuilder = new StringBuilder();
		// Start string text.
		stringBuilder.append(graphName);
		stringBuilder.append(".addVertex(");
		// add each pair, quote around all but 'T.id' and 'T.label' which are
		// "accessor tokens".
		for (Map.Entry<String, String> entry : pairs) {
			boolean
							noQuoteFlag =
							(entry.getKey()
							 == vertexId)
							|| (entry.getKey()
							    == vertexLabel);
			stringBuilder.append((noQuoteFlag ? "" : "\""))
			             .append(entry.getKey())
			             .append(
							             (noQuoteFlag ? "" : "\""))
			             .append(", ")
			             .append("\"")
			             .append(entry.getValue())
			             .append("\", ");
		}
		// add last ')'.
		stringBuilder.append(")");
		// combine all into single String & return.
		return stringBuilder.toString();
	}


}
