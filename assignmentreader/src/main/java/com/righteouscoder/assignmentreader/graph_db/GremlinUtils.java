package com.righteouscoder.assignmentreader.graph_db;

import org.apache.tinkerpop.gremlin.driver.Client;
import org.apache.tinkerpop.gremlin.driver.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

/**
 * Class for containing static utility methods for interacting with gremlin for
 * Graph DBs.
 */
@SuppressWarnings("WeakerAccess")
public class GremlinUtils {

	private static final Logger
					logger =
					LoggerFactory.getLogger(GremlinUtils.class);

	// how long to sleep to remove race condition on output of results
	public static final long DEFAULT_EXAMPLE_SLEEP_TIME = 200;

	// query to give gremlin on each example.
	public static final String DEFAULT_EXAMPLE_GREMLIN_QUERY = "g.V()";

	public static List<Result> printResults(List<Result> results) {
		for (Result result :
						results) {
			GremlinUtils.logger.info(""
			                         + result.getString());
		}
		return results;
	}

	public static List<Result> printResults(String gremlinQuery,
	                                 List<Result> results) {
		GremlinUtils.logger.info("gremlin query: "
		                         + gremlinQuery);
		for (Result result :
						results) {
			GremlinUtils.logger.info(""
			                         + result.getString());
		}
		return results;
	}

	static void printGremlinExamples(Client client)
					throws
					Exception {
		printGremlinExamples(client,
		                     DEFAULT_EXAMPLE_GREMLIN_QUERY,
		                     DEFAULT_EXAMPLE_SLEEP_TIME);
	}

	static void printGremlinExamples(Client client,
	                                 String gremlinQuery)
					throws
					Exception {
		printGremlinExamples(client,
		                     gremlinQuery,
		                     DEFAULT_EXAMPLE_SLEEP_TIME);
	}

	public static void printGremlinExamples(Client client,
	                                 String gremlinQuery,
	                                 long sleepTimeMS)
					throws
					Exception {
		/*------------------------------------------------------------------*/
		/*- Example 1, Synchronous CompletableFuture usage.                -*/
		/*------------------------------------------------------------------*/

		logger.debug("start examples");
		logger.debug(" before query 1");
		/*
		 * This is how you make a CompletableFuture
		 */
		CompletableFuture<List<Result>>
						all =
						client.submit(gremlinQuery)
						      .all();
		// This is how you make the thread wait until the results are complete.
		all.get();

		// note there is 'get(...)' with timeout options.
		// Here is an example of one such timeout 2,000ms = 2 seconds.
		// all.get(2000, TimeUnit.MILLISECONDS);

		// This is how you use lambda expression to print the results.
		all.thenAccept(results -> {
			logger.debug("gremlinQuery = "
			             + gremlinQuery);
			GremlinUtils.printResults(results);
		});
		logger.debug(" after query 1");

		/*------------------------------------------------------------------*/
		/*- Example 2, using functional consumer                           -*/
		/*------------------------------------------------------------------*/

		logger.debug(" before query 2");
		/*
		 * This is how you make a CompletableFuture
		 */
		CompletableFuture<List<Result>>
						all2 =
						client.submit(gremlinQuery)
						      .all();
		// This is how you make the thread wait until the results are complete.
		// note there is 'get(...)' with timeout options.
		all2.get();

		// This is how you function name to list static (mono)consumer method.
		all2.thenAccept(GremlinUtils::printResults);
		logger.debug(" after query 2");

		/*------------------------------------------------------------------*/
		/*- Example 3, using functional consumer  w/ 2 params              -*/
		/*------------------------------------------------------------------*/

		logger.debug(" before query 3");
		/*
		 * This is how you make a CompletableFuture
		 */
		CompletableFuture<List<Result>>
						all3 =
						client.submit(gremlinQuery)
						      .all();
		// This is how you make the thread wait until the results are complete.
		// note there is 'get(...)' with timeout options.
		all3.get();

		// This is how you function name to list static (mono)consumer method.
		all3.thenAccept(results -> {
			GremlinUtils.printResults(gremlinQuery,
			                          results);
		});
		logger.debug(" after query 3");

		/*------------------------------------------------------------------*/
		/*- Example 4, Same as 3, but Asynchronous printing results        -*/
		/*------------------------------------------------------------------*/

		logger.debug(" before query 4");
		    /*
		     * This is how you make a CompletableFuture
         */
		CompletableFuture<List<Result>>
						all4 =
						client.submit(gremlinQuery)
						      .all();

		// This is how you function name to list static (mono)consumer method.
		all4.thenAccept(results -> {
			GremlinUtils.printResults(gremlinQuery,
			                          results);
		});
		logger.debug(" after query 4");

		/*------------------------------------------------------------------*/
		/*- Example 5, Same as 4, refactored to decrease code              -*/
		/*-     Note that Functional Programming (FP) does NOT always      -*/
		/*-     help with readability, & still requires careful/extensive  -*/
		/*-     documentation                                              -*/
		/*------------------------------------------------------------------*/

		logger.debug(" before query 5");
		/*
		 * Single command to query the client, wait for results, and then
		 * print the results out with the query that created them.
		 */
		client.submit(gremlinQuery)
		      .all()
		      .thenAccept(results -> {
			      GremlinUtils.printResults(gremlinQuery,
			                                results);
		      });
		logger.debug(" after query 5");

		/*------------------------------------------------------------------*/
		/*- Example 6, Same as 5, but now synchronous                      -*/
		/*------------------------------------------------------------------*/

		logger.debug(" before query 6");
		/*
		 * Single command to query the client, wait for results, and then
		 * print the results out with the query that created them.
		 */
		client.submit(gremlinQuery)
		      .all()
		      .thenAccept(results -> {
			      GremlinUtils.printResults(gremlinQuery,
			                                results);
		      })
		      .get();
		logger.debug(" after query 6");

		/*------------------------------------------------------------------*/
		/*- Example 7, Same as 6, but now with 2 second delay              -*/
		/*-                to (help) prevent race condition on output      -*/
		/*------------------------------------------------------------------*/

		Thread.sleep(sleepTimeMS);
		logger.debug(" Slept for "
		             + (sleepTimeMS)
		             + "ms");

		logger.debug(" before query 7");
		/*
		 * Single command to query the client, wait for results, and then
		 * print the results out without the query that created them.
		 */
		client.submit(gremlinQuery)
		      .all()
		      .thenAccept(results -> {
			      GremlinUtils.printResults(gremlinQuery,
			                                results);
		      })
		      .get();
		logger.debug(" after query 7");

		/*------------------------------------------------------------------*/
		/*- Example 8, Same as 7, but (Synchronous)'One-Liner" now,        -*/
		/*-                       if no desire to print query              -*/
		/*------------------------------------------------------------------*/

		Thread.sleep(sleepTimeMS);
		logger.debug(" Slept for "
		             + (sleepTimeMS)
		             + "ms");

		logger.debug(" before query 8");
		/*
		 * Single command to query the client, wait for results, and then
		 * print the results out without the query that created them.
		 */
		client.submit(gremlinQuery)
		      .all()
		      .thenAccept(GremlinUtils::printResults)
		      .get();
		logger.debug(" after query 8");

		/*------------------------------------------------------------------*/
		/*- Example 9, Same as 8, but now Asynchronous)                    -*/
		/*------------------------------------------------------------------*/

		Thread.sleep(sleepTimeMS);
		logger.debug(" Slept for "
		             + (sleepTimeMS)
		             + "ms");

		logger.debug(" before query 9");
		/*
		 * Single command to query the client, wait for results, and then
		 * print the results out without the query that created them.
		 */
		client.submit(gremlinQuery)
		      .all()
		      .thenAccept(GremlinUtils::printResults);
		logger.debug(" after query 9");

		/*------------------------------------------------------------------*/
		/*- Example 10, Use Java8 streams methods                          -*/
		/*------------------------------------------------------------------*/

		Thread.sleep(sleepTimeMS);
		logger.debug(" Slept for "
		             + (sleepTimeMS)
		             + "ms");

		logger.debug(" before query 10");
		/*
		 * Single command to query the client, wait for results, and then
		 * print the results out without the query that created them.
		 */
		client.submit(gremlinQuery)
		      .all()
		      .get()
		      .stream()
		      .map(result -> result.getString())
		      .collect(Collectors.toList())
		      .stream()
		      .forEach(GremlinUtils::logString);
		logger.debug(" after query 10");

		// End the examples

		logger.debug("end examples");


		client.submit("g.V().has('age', gt(30)).as('node').values('name').as"
		              + "('name').select('node','name')")
		      .all()
		      .get()
		      .stream()
		      .map(result -> result.getString())
		      .collect(Collectors.toList())
		      .stream()
		      .forEach(GremlinUtils::logString);

	}

	private static void logString(String string) {
		logger.debug(string);
	}
}
