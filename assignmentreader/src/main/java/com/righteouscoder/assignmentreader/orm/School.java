package com.righteouscoder.assignmentreader.orm;

/**
 * POJO class to represent a 'School' in the graph DB.
 */
@SuppressWarnings("unused")
public class School
				extends Vertex {

	private String name1;
	private String name2;
	private String streetAddress1;
	private String streetAddress2;
	private String streetAddress3;
	private String city;
	private String stateProvince;
	private String country;
	private String postalCode;
	private String phoneNumber1;
	private String phoneNumber2;
	private String email1;
	private String email2;

	public String getName1() {
		return name1;
	}

	public School setName1(String name1) {
		this.name1 = name1;
		return this;
	}

	public String getName2() {
		return name2;
	}

	public School setName2(String name2) {
		this.name2 = name2;
		return this;
	}

	public String getStreetAddress1() {
		return streetAddress1;
	}

	public School setStreetAddress1(String streetAddress1) {
		this.streetAddress1 = streetAddress1;
		return this;
	}

	public String getStreetAddress2() {
		return streetAddress2;
	}

	public School setStreetAddress2(String streetAddress2) {
		this.streetAddress2 = streetAddress2;
		return this;
	}

	public String getStreetAddress3() {
		return streetAddress3;
	}

	public School setStreetAddress3(String streetAddress3) {
		this.streetAddress3 = streetAddress3;
		return this;
	}

	public String getCity() {
		return city;
	}

	public School setCity(String city) {
		this.city = city;
		return this;
	}

	public String getStateProvince() {
		return stateProvince;
	}

	public School setStateProvince(String stateProvince) {
		this.stateProvince = stateProvince;
		return this;
	}

	public String getCountry() {
		return country;
	}

	public School setCountry(String country) {
		this.country = country;
		return this;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public School setPostalCode(String postalCode) {
		this.postalCode = postalCode;
		return this;
	}

	public String getPhoneNumber1() {
		return phoneNumber1;
	}

	public School setPhoneNumber1(String phoneNumber1) {
		this.phoneNumber1 = phoneNumber1;
		return this;
	}

	public String getPhoneNumber2() {
		return phoneNumber2;
	}

	public School setPhoneNumber2(String phoneNumber2) {
		this.phoneNumber2 = phoneNumber2;
		return this;
	}

	public String getEmail1() {
		return email1;
	}

	public School setEmail1(String email1) {
		this.email1 = email1;
		return this;
	}

	public String getEmail2() {
		return email2;
	}

	public School setEmail2(String email2) {
		this.email2 = email2;
		return this;
	}
}
