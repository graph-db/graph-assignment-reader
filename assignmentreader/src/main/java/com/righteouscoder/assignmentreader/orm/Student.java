package com.righteouscoder.assignmentreader.orm;

/**
 *
 */
public class Student
				extends Vertex {

	private String studentID;
	private String firstName;
	private String middleName;
	private String lastName;

	public String getStudentID() {
		return studentID;
	}

	public Student setStudentID(String studentID) {
		this.studentID = studentID;
		return this;
	}

	public String getFirstName() {
		return firstName;
	}

	public Student setFirstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	public String getMiddleName() {
		return middleName;
	}

	public Student setMiddleName(String middleName) {
		this.middleName = middleName;
		return this;
	}

	public String getLastName() {
		return lastName;
	}

	public Student setLastName(String lastName) {
		this.lastName = lastName;
		return this;
	}
}
