package com.righteouscoder.assignmentreader.orm;

/**
 * Base class for all ORM objects for GraphDB
 */
abstract class Vertex {

	String vertexLabel = null;

	public String getVertexLabel() {
		return vertexLabel;
	}

	public Vertex setVertexLabel(String vertexLabel) {
		this.vertexLabel = vertexLabel;
		return this;
	}

	String vertexId = null;

	public Vertex setVertexId(String vertexId) {
		this.vertexId = vertexId;
		return this;
	}

	public String getVertexId() {
		return vertexId;
	}

}
