package com.righteouscoder.assignmentreader.logic;

/**
 * Main Entry point for the Application.
 */
public class Main {

	/**
	 * Starting point for the application. Takes command line args and prepares
	 * them for processing.
	 *
	 * @param args
	 * 				Strings to use as arguments.
	 */
	public static void main(String[] args) {

		// could do full/proper CLI parsing here. 
		AssignmentReader main = new AssignmentReader();
		main.run(args);
	}

}
