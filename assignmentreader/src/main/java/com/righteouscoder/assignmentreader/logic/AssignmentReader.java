package com.righteouscoder.assignmentreader.logic;

import com.righteouscoder.assignmentreader.io.FileIO;
import com.righteouscoder.assignmentreader.net.Service;
import com.righteouscoder.assignmentreader.parser.ParserUtils;
import com.righteouscoder.assignmentreader.crypto.Crypto;
import com.righteouscoder.assignmentreader.crypto.CryptoImpl;
import com.righteouscoder.assignmentreader.io.FileIOImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

public class AssignmentReader {

	private static final Logger logger =
					LoggerFactory.getLogger(AssignmentReader.class);


	public void run(String args[]) {

		//System.out.println(System.getProperty("user.dir"));
		Service service = Service.getInstance();
		try {
			logger.info("Sending request....");

			// This will run the gremlin test.
//			testGremlinService(service);

			// This will run the java8 parsing test.
			testJava8Parsing();


		} catch (Exception ex) {
			logger.error("Could not execute traversal",
			             ex);
		} finally {
			try {
				// Closing of the service closes all associated clients too
				// automatically.
				service.close();
			} catch (Exception e) {
				logger.error("Exception on service close: "
				             + e.getMessage());
			}
			logger.info("Service closed and resources released");
		}
	}

	void testGremlinService(Service service )
					throws
					Exception {
		// This will run the gremlin test with the provided query for each test.
		service.testGremlinAPI("g.V()");
	}

	void testJava8Parsing()
					throws
					IOException {

			String[] inputs = new String[]{System.getProperty("user.dir")
			                               +
			                               "/parser/src/test/samples/LogicUnitTests"
			                               + ".java"};

			FileIO fileIO = new FileIOImpl();

			InputStream is = fileIO.getParserTestFile();

			Crypto crypto = new CryptoImpl();

			logger.debug("sha1 of input file: "
			             + crypto.sha1Hex(is));

			is = fileIO.getParserTestFile();


			logger.debug("available bytes: "
			             + is.available());


			ParserUtils.test(fileIO.getParserTestFile());
	}

      /*

        Date today = new Date();
        System.out.println(today);


        Cluster cluster = Cluster.open();

        Client client = cluster.connect();


        // 1
        ResultSet results = client.submit("[1,2,3,4]");
        // 2
        //results.stream().map(i -> i.get(Integer.class) * 2);

        System.out.println(results.getAvailableItemCount());


/*
        // 3
        CompletableFuture<List<Result>> results = client.submit("[1,2,3,4]")
        .all();

        // 4
        CompletableFuture<ResultSet> future = client.submitAsync("[1,2,3,4]");

        // 5
        Map<String,Object> params = new HashMap<>();
        params.put("x",4);
        client.submit("[1,2,3,x]", params);
*/


}
