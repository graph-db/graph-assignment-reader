package com.righteouscoder.assignmentreader.crypto;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.IOException;
import java.io.InputStream;


/**
 *
 */
@SuppressWarnings("unused")
public class CryptoImpl
				implements Crypto {

	public String sha1Hex(String input) {
		return DigestUtils.sha1Hex(input);
	}



	public String sha1Hex(InputStream input)
					throws
					IOException {
		return DigestUtils.sha1Hex(input);
	}

}
