package com.righteouscoder.assignmentreader.crypto;

import java.io.IOException;
import java.io.InputStream;

/**
 *
 */
@SuppressWarnings({"unused", "UnnecessaryInterfaceModifier"})
public interface Crypto {

	public String sha1Hex(String input);

	public String sha1Hex(InputStream input)
					throws
					IOException;

}
