package com.righteouscoder.assignmentreader.misc;

/**
 *
 */
public class DefaultConstants {

	public final static String TINKERPOP_DRIVER_SETTINGS_NAME = "driver-settings"
	                                                     + ".yaml";

	public final static String PARSER_TEST_FILE_NAME = "SampleJava8File.java";


}
