package com.righteouscoder.assignmentreader.net;

import com.righteouscoder.assignmentreader.graph_db.GremlinUtils;
import com.righteouscoder.assignmentreader.io.FileIO;
import com.righteouscoder.assignmentreader.io.FileIOImpl;
import org.apache.tinkerpop.gremlin.driver.Client;
import org.apache.tinkerpop.gremlin.driver.Cluster;
import org.apache.tinkerpop.gremlin.driver.Result;
import org.apache.tinkerpop.gremlin.driver.ResultSet;
import org.apache.tinkerpop.gremlin.driver.remote.DriverRemoteConnection;
import org.apache.tinkerpop.gremlin.process.traversal.Bindings;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph
				.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Graph;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.apache.tinkerpop.gremlin.structure.util.empty.EmptyGraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.AbstractMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class Service
				implements AutoCloseable {

	private static final Logger logger = LoggerFactory.getLogger(Service.class);

	/**
	 * There typically needs to be only one Cluster instance in an application.
	 */
	private Cluster cluster;

	/**
	 * Use the Cluster instance to construct different Client instances (e.g. one
	 * for sessionless communication and one or more sessions). A sessionless
	 * Client should be thread-safe and typically no more than one is needed
	 * unless there is some need to divide connection pools across multiple
	 * Client
	 * instances. In this case there is just a single sessionless Client instance
	 * used for the entire App.
	 */
	private Client client;

	/**
	 * Create Service as a singleton given the simplicity of App.
	 */
	private static final Service INSTANCE = new Service();

	public final String driverSettingsLocation = "driver-settings.yaml";

	private Service() {
		try {

			// get instance of FileIO
			FileIO fileIO = new FileIOImpl();

			// build the cluster based off of the Default Config File
			cluster =
							Cluster.build(fileIO.getTinkerpopConfigFile())
							       .create();
			// connect a client to the cluster.
			client = cluster.connect();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return
	 */
	public static Service getInstance() {
		return INSTANCE;
	}

	/**
	 * @throws Exception
	 */
	public void nativeAPIWithRemote()
					throws
					Exception {

		Graph graph = EmptyGraph.instance();

		Bindings b = Bindings.instance();

		GraphTraversalSource g = graph
						.traversal()
						.withRemote(DriverRemoteConnection.using(cluster,
						                                         "g"));

		GraphTraversal<Vertex, Vertex> v = g.V("1");

		Thread.sleep(2000);


		while (v.hasNext()) {
			logger.debug(" next =  "
			             + v.next());
		}


//        v.forEachRemaining(vertex -> {
//            logger.debug("vertex" + vertex.toString());
//        });
//

//        GraphTraversal<Vertex, Long> graphTraversal = g.V(1).properties
// ("name").count();

//        GraphTraversal<Vertex, ? extends Property<Object>> name = g.V(1)
// .properties("name");

//        GraphTraversal<Vertex, Long> count = name.count();

		// graphTraversal.value();

//        while (name.properties("name").hasNext()) {
//            Property<Object> name1 = name.properties("name").next();
//            logger.debug("answer : " + name1.toString());
//        }

//        logger.debug("answer : " + name.toString() );
		// logger.info("graphTraversal.count: "+graphTraversal.count());
		// logger.info("graphTraversal.hasNext: "+graphTraversal.hasNext());

//        while (graphTraversal.hasNext()){
//            logger.info("graphTraversal.count: "+graphTraversal.next());
//        }

		// doesn't work: "Graph does not support graph variables"
		// logger.info("graphTraversal.count: "+graph.variables().keys().size());
	}

	public List<String> testGremlinAPI(String gremlinQuery)
					throws
					Exception {


		client.init();

		GremlinUtils.printGremlinExamples(client,
		                                  gremlinQuery,
		                                  GremlinUtils.DEFAULT_EXAMPLE_SLEEP_TIME);


//        try {
//            Thread.sleep(2000);
//        } catch (Exception ex) {
//            logger.error("Error trying to sleep");
//        }


//        logger.debug("Available Item Count: " +
// results3.getAvailableItemCount());
//
//        logger.info("All Items Available: " + results3.allItemsAvailable());
//
//        CompletableFuture<List<Result>> completableResultsList =
// results3.all();
//
//
//        logger.info("Completable Future is done: " + completableResultsList
// .isDone());
//        List<Result> resultList = completableResultsList.get();
//        logger.info("Result List Size: " + resultList.size());
//
//        for (Result result :
//                resultList) {
//            logger.info("result String : " + result.getString());
//        }
//
//        String getV1name = "g.V(1).values('name')";
//
//
//        logger.info("g.V(1).values('name'): " + getResultString("g.V(1)
// .values('name')").get(0).getString());
//
//        //logger.info("g.V(1).values('name'): " + getResultString("g.V(1)
// .values('name')").get(0)
//        //      .getString());
//
//        queryGremlinScriptAndPrint("g.V(1).values('name')");

		//   ResultSet resultAdd = client.submit("graph.addVertex(T.label,
		// \"software\",\n \"name\", \"gremlin\")");

//        List<java.util.Map.Entry<String, String>> touple = new ArrayList<>();
//
//        touple.add(createEntry("T.label", "software"));
//        touple.add(createEntry("name", "gremlin-server"));
//
//
//        CompletableFuture<Void> voidCompletableFuture = addVertex(touple)
//                .all()
//                .thenAccept(this::printResults);
//
//        CompletableFuture<CompletableFuture<Void>> async =
// CompletableFuture.supplyAsync(() -> {
//                    return client.submit("g.V()").all().thenAccept
// (this::printResults);
//                }
//        );


//        voidCompletableFuture.thenComposeAsync(async);

//
//
//        resultAdd.all().thenAccept(results ->
//        {
//            results.forEach(result ->
//            {
//                logger.info("results add each result: " + result);
//            });
//        });

		//CompletableFuture<List<Result>> results2 = client.submit("[1,2,3,4]")
		// .all();

		return null;
	}


	private java.util.Map.Entry<String, String> createEntry(String key,
	                                                        String value) {
		java.util.Map.Entry<String, String>
						entry =
						new AbstractMap.SimpleEntry<>(key,
						                              value);
		return entry;
	}

	public ResultSet addVertex(List<java.util.Map.Entry<String, String>> pairs) {

		String queryString = "graph.addVertex(";

		for (java.util.Map.Entry<String, String> entry : pairs) {

			boolean quoteKey =
							(entry.getKey()
							 == "T.id")
							|| (entry.getKey()
							    == "T.label");

			queryString += (quoteKey ? "" : "\"")
			               + entry.getKey()
			               + (quoteKey ? "" : "\"")
			               + ", "
			               + "\""
			               + entry.getValue()
			               + "\", ";
		}
		queryString += ")";

		ResultSet resultAdd = client.submit(queryString);

		return resultAdd;

	}

	private String queryGremlinScriptAndPrint(String inputString)
					throws
					Exception {

		client.submit(inputString) // ResultSet
		      .all() // List<Result>
		      .thenAccept( // consume List<Result>
		                   GremlinUtils::printResults
		      );
		return inputString;
	}


	private List<Result> getResultString(String inputString)
					throws
					Exception {
		ResultSet results3 = client.submit("g.V()");

		try {
			Thread.sleep(2000);
		} catch (Exception ex) {
			logger.error("Error trying to sleep");
		}
		CompletableFuture<List<Result>> completableResultsList = results3.all();


//            completableResultsList.thenAccept(list -> {
//                for (Result result :
//                        list) {
//                    logger.info("" + result.getString());
//                }
//        });

		//
		List<Result> resultList = completableResultsList.get();

//        for (Result result :
//                resultList) {
//            logger.info("result String : "  + result.getString() );
//        }

		return resultList;
	}

	@Override
	public void close()
					throws
					Exception {
		client.close();
		cluster.close();
	}
}